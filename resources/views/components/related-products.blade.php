@if($relatedProducts->count() > 0)
    <div class="related-products-container">
        <h2>Productos relacionados</h2>
        <div class="related-products">
            @foreach($relatedProducts as $relatedProduct)
                @livewire('bpanel4-products::public.product-small', ['productId' => $relatedProduct->getId()],
                key($relatedProduct->id))
            @endforeach
        </div>
    </div>
@endif