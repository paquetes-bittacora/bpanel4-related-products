<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedProducts\Services;

use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Database\Eloquent\Builder ;
use Illuminate\Database\Eloquent\Collection;

class RelatedProductsService
{
    /**
     * @return Collection<array-key, Product>
     */
    public function findRelatedProducts(Product $product, int $numberOfProducts = 4): Collection
    {
        $categoryIds = $this->getCategoryIds($product);

        return $this->getQuery($categoryIds, $product)->get()->shuffle()->splice(0, $numberOfProducts);
    }

    /**
     * @return array<int>
     */
    private function getCategoryIds(Product $product): array
    {
        $categories = $product->getCategories();
        $categoryIds = [];
        foreach ($categories as $category) {
            $categoryIds[] = $category->getId();
        }
        return $categoryIds;
    }

    protected function getQuery(array $categoryIds, Product $product): Builder
    {
        return Product::whereHas('categorizable', static function (Builder $query) use ($categoryIds): void {
            $query->whereIn('category_id', $categoryIds);
        })->where('id', '<>', $product->getId())->where('active', true);
    }
}
