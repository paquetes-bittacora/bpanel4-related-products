<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedProducts;

use Bittacora\Bpanel4\RelatedProducts\View\RelatedProductsComponent;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;

final class Bpanel4RelatedProductsServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'related-products';

    public function boot(BladeCompiler $blade): void
    {
        $blade->component(RelatedProductsComponent::class, self::PACKAGE_PREFIX);
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
    }
}
