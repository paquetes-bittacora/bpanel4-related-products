<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\RelatedProducts\View;

use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\RelatedProducts\Services\RelatedProductsService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

final class RelatedProductsComponent extends Component
{
    public function __construct(
        private readonly Factory $view,
        private readonly RelatedProductsService $relatedProductsService,
        private readonly Product $product,
    ) {
    }

    public function render(): View
    {
        return $this->view->make('related-products::components.related-products', [
            'relatedProducts' => $this->relatedProductsService->findRelatedProducts($this->product),
        ]);
    }
}
